import { Scheduler } from './scheduler.js'


const scheduler = new Scheduler()

document.addEventListener("DOMContentLoaded", function(event) {
    let addEventBtn = document.getElementById('add-event-btn')
    addEventBtn.addEventListener('click', scheduler.addEvent.bind(scheduler))
});

