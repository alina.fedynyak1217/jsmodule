import { eventItems } from './eventItems.js'

const timeData = [
    0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360, 390, 420, 450, 480, 510, 540
]

export class Scheduler {
    constructor() {
        this.schedulerBlock = document.getElementById('scheduler')
        let events = JSON.parse(localStorage.getItem('events'))
        this.eventItems = events !== null ? events : eventItems
        this.init(this.eventItems)
    }

    init(eventItems) {

        eventItems = eventItems.map((item, itemIndex) => {
            let conflict = false
            eventItems.forEach((event, eventIndex) => {
                if (itemIndex === eventIndex) {
                    return
                }
                let itemTimeEnd = item.start + item.duration
                let eventTimeEnd = event.start + event.duration

                if ((event.start <= item.start && item.start < eventTimeEnd) || (itemTimeEnd >= event.start && itemTimeEnd <= eventTimeEnd)) {
                    conflict = true
                }
            })
            item.conflict = conflict
            return item
        })
        console.log(eventItems)

        this.schedulerBlock.innerText = ''
        let schedulerHtml = this.createSchedulerHtml(eventItems)
        this.schedulerBlock.appendChild(schedulerHtml)
        localStorage.setItem('events', JSON.stringify(eventItems))
    }

    createSchedulerHtml(eventItems) {
        let schedulerContainer = document.createElement('div')
        schedulerContainer.className = 'calendar__container'

        let preventEventsTime = 0
        timeData.forEach((time) => {
            let fullHour = time % 60
            let hourClass = fullHour === 0 ? 'big-hour' : 'little-hour'
            let eventBlock = document.createElement('div')
            eventBlock.className = 'item ' + hourClass
            eventBlock.setAttribute('style','height: 60px')
            let timeBlock = document.createElement('div')
            timeBlock.innerHTML = `<span class="` + hourClass + `">` + this.convertToTime(time) + `</span>`
            eventBlock.appendChild(timeBlock)
            let eventContainer = document.createElement('div')
            eventContainer.className = 'event-container'
            let events = this.findEvents(time, eventItems)
            let preventEvents = this.findEvents(preventEventsTime, eventItems)
            preventEventsTime = time

            events.forEach((event, index) => {
                let eventElement = document.createElement('div')
                eventElement.className = 'title'
                eventElement.id = 'event-' + event.start
                eventElement.style['height'] = event.duration * 2 + 'px';
                eventElement.style['position'] = 'absolute'
                if (event.color) {
                    eventElement.style['background'] = event.color
                    eventElement.style['opacity'] = '0.9'
                }

                let startDuration = event.start - time
                eventElement.style['margin-top'] = startDuration * 2 + 'px'

                let stringLength = 20

                if (event.conflict) {
                    eventElement.style['width'] = '90px'
                    if (index >= 1 ) {
                        eventElement.style['left'] =  177 + 'px'
                    }
                    stringLength = 10
                }

                let eventContent = document.createElement('div')
                let eventText = event.title.substr(0, stringLength)
                if (eventText.length < event.title.length) {
                    eventText += '...'
                }

                eventContent.innerHTML = eventText

                let removeBtn = document.createElement('span')
                removeBtn.className = 'remove'
                removeBtn.setAttribute('data-event-id', event.start)
                removeBtn.addEventListener('click', this.removeEvent.bind(this))

                let editBtn = document.createElement('span')
                editBtn.className = 'edit'
                editBtn.setAttribute('data-event-id', event.start)
                editBtn.addEventListener('click', this.editEvent.bind(this))

                eventElement.appendChild(eventContent)
                eventElement.appendChild(editBtn)
                eventElement.appendChild(removeBtn)
                eventContainer.appendChild(eventElement)
            })

            eventBlock.appendChild(eventContainer)
            schedulerContainer.appendChild(eventBlock)
        })

        return schedulerContainer
    }

    editEvent(e) {
        let eventId = e.currentTarget.getAttribute('data-event-id')
        let events = JSON.parse(localStorage.getItem('events'))
        this.eventItems = events !== null ? events : eventItems
        let editItem = this.eventItems.filter((event) => {
            if(event.start === +eventId) {
                return true
            }
        }).shift()

        let blockBtn = document.getElementsByClassName ('my-form__btn')[0]
        let editBtn = document.createElement('button')
        editBtn.id = 'edit-event-btn'
        editBtn.innerText = 'Edit event'
        editBtn.setAttribute('data-event-id', editItem.start)
        editBtn.addEventListener('click', this.editItem.bind(this))
        blockBtn.appendChild(editBtn)

        let startTimeEventElement = document.getElementById('event-time-start')
        let endTimeEventElement = document.getElementById('event-time-end')
        let textEventElement = document.getElementById('event-text')
        let colorEventElement = document.getElementById('event-color')

        startTimeEventElement.value = this.convertToTime(editItem.start)
        endTimeEventElement.value = this.convertToTime(editItem.start + editItem.duration)
        textEventElement.value = editItem.title
        colorEventElement.value = editItem.color ? editItem.color : '#E2ECF5'
    }

    editItem(e) {
        e.preventDefault()
        let eventId = e.currentTarget.getAttribute('data-event-id')
        e.currentTarget.remove()
        let startTimeEventElement = document.getElementById('event-time-start')
        let endTimeEventElement = document.getElementById('event-time-end')
        let textEventElement = document.getElementById('event-text')
        let colorEvenetElement = document.getElementById('event-color')
        this.eventItems = JSON.parse(localStorage.getItem('events'))
        let startTimeData = startTimeEventElement.value.split(':')
        let startTime =  (parseInt(startTimeData[0]) * 60 + parseInt(startTimeData[1])) - 480

        if (startTime < 0 || startTime > 540) {
            alert('Начало события возможно с 8:00 до 17:00')
            return;
        }

        let endTimeData = endTimeEventElement.value.split(':')
        let endTime =  (parseInt(endTimeData[0]) * 60 + parseInt(endTimeData[1])) - 480

        if (endTime < startTime) {
            alert('Не правильно указано время. Возможно указать время с 8:00 до 17:00 ')
            return;
        }

        if (endTime < 0 || endTime > 540) {
            alert('Конец события возможн с 8:00 до 17:00')
            return;
        }

        let duration = endTime - startTime

        this.eventItems = this.eventItems.map(((item) => {
            if (item.start === +eventId) {
                item.start = startTime
                item.duration = duration
                item.title = textEventElement.value
                item.color = colorEvenetElement.value
            }
            return item
        }))

        this.init(this.eventItems)
    }

    removeEvent(e) {
        let eventId = e.currentTarget.getAttribute('data-event-id')

        let events = JSON.parse(localStorage.getItem('events'))
        this.eventItems = events !== null ? events : eventItems
        this.eventItems = this.eventItems.filter((event) => {
            if(event.start !== +eventId) {
                return true
            }
        })

        localStorage.setItem('events', JSON.stringify(this.eventItems))

        let currentEvent = document.getElementById('event-' + eventId)
        currentEvent.remove()
        this.init(this.eventItems);
    }

    convertToTime(value) {
        let start = 480 + value // 8 hours to minutes plus another time
        let hours = Math.floor(start / 60);
        let minutes = start % 60;
        if (hours < 10) {
            hours = '0' + hours
        }
        if (minutes < 10) {
            minutes = minutes + '0'
        }
        return `${hours}:${minutes}`;
    }

    findEvents(startTime, events){
        return events.filter((event) => {
            let endTime = startTime + 30
            if (event.start >= startTime && event.start < endTime) {
                return true
            }
        })
    }

    addEvent(e) {
        e.preventDefault()
        let startTimeEventElement = document.getElementById('event-time-start')
        let endTimeEventElement = document.getElementById('event-time-end')
        let textEventElement = document.getElementById('event-text')
        let colorEvenetElement = document.getElementById('event-color')


        this.eventItems = JSON.parse(localStorage.getItem('events'))

        let startTimeData = startTimeEventElement.value.split(':')
        let startTime =  (parseInt(startTimeData[0]) * 60 + parseInt(startTimeData[1])) - 480

        if (startTime < 0 || startTime > 540) {
            alert('Начало события возможно с 8:00 до 17:00')
            return;
        }

        let endTimeData = endTimeEventElement.value.split(':')
        let endTime =  (parseInt(endTimeData[0]) * 60 + parseInt(endTimeData[1])) - 480

        if (endTime < startTime) {
            alert('Не правильно указано время. Возможно указать время с 8:00 до 17:00 ')
            return;
        }

        if (endTime < 0 || endTime > 540) {
            alert('Конец события возможн с 8:00 до 17:00')
            return;
        }

        let duration = endTime - startTime

        let newEvent = {
            start: startTime,
            duration: duration,
            title: textEventElement.value,
            color: colorEvenetElement.value
        }

        textEventElement.value = ''
        startTimeEventElement.value = '08:00'
        endTimeEventElement.value = '09:00'
        colorEvenetElement.value = '#E2ECF5'

        this.eventItems.push(newEvent)
        this.init(this.eventItems)
    }
}
